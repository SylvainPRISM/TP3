// Port to listen requests from
var port = 1234;

// Modules to be used
var express = require('express');
var bodyParser = require('body-parser');
var sqlite3 = require('sqlite3').verbose();
var app = express();
var db = new sqlite3.Database('db.sqlite');

var jwt = require('jwt-simple');
app.set('jwtTokenSecret', 'YOUR_SECRET_STRING');
var md5 = require('md5');
// application/x-www-form-urlencoded parser
var urlencodedParser = bodyParser.urlencoded({ extended: false });

// Log requests
app.all("*", urlencodedParser, function(req, res, next){
	console.log(req.method + " " + req.url);
	console.dir(req.headers);
	console.log(req.body);
	console.log();
	next();
});

// Serve static files
app.use(express.static('public'));

app.get("/users", function(req, res, next) {
	db.all('SELECT rowid, ident, password FROM users;', function(err, data) {
		res.json(data);
	});
});
app.get("/sessions", function(req, res, next) {
	db.all('SELECT * FROM sessions;', function(err, data) {
		res.json(data);
	});
});


app.post("/login", function(req, res, next) {
    // console.dir(req.body);
    var randomnumber=Math.floor(Math.random()*21);
    db.get('SELECT rowid, ident, password FROM users WHERE ident = ? AND password = ?;',[req.body.ident, md5(req.body.password)], function(err,data){
	if (data){
	    var token = jwt.encode({
		ident: req.body.ident,
		password: req.body.password
	    }, app.get('jwtTokenSecret'));

	    db.get('SELECT count(token) AS nb FROM sessions WHERE ident = "admin";', function(err,data){
	    	if (data.nb != 0){
		    res.cookie("session",token);
		    console.dir(data);
		    res.send("Connection OK ! ");
		    token = jwt.encode({
			ident: req.body.ident,
			password: req.body.password+randomnumber
		    }, app.get('jwtTokenSecret'));
		    db.run('UPDATE sessions set token=? WHERE ident=?',[token,req.body.ident]);
		    console.dir(token);
		}
		else {
		    db.run('INSERT INTO sessions (ident,token) VALUES (?,?)',[req.body.ident,token]);
		    res.json({
			status: true,
			token: token    
		    });		
		}		
	    });
	
	}
	else{
	    res.send("ERREUR DE CONNEXION");
	    res.status(400);
	}});
    
    
});

// Startup server
app.listen(port, function () {
	console.log('Le serveur est accessible sur http://localhost:' + port + "/");
	console.log('La liste des utilisateurs est accessible sur http://localhost:' + port + "/users");
});
